import React from 'react'

function IconChevron({
  direction = 'down',
  id = 'RSChevron',
  title = 'RS Chevron',
  className = 'RS-icon-chevron',
  fill = '#fff',
  width = '34.2',
  height = '19'
}) {
  const paths = {
    down: 'M550,175L300,425L50,175h127l123,123.1L423.1,175H550z',
    up: 'M423.1,425L300,301.9L177,425H50l250-250l250,250H423.1z',
    left: 'M425,175L300,300.7L425,425v125L174.8,300L425,50V175z',
    right: 'M175,425l125-125.7L175,175V50l250.2,250L175,550V425z'
  }
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 600 600"
      width={width}
      height={height}
      aria-labelledby={id}
      className={className}
    >
      <title id={id}>{title}</title>
      <path fill={fill} d={paths[direction]} />}
    </svg>
  )
}

export default IconChevron