import React from 'react'

const Node = ({ header, nodes, prependUrl }) => {
  return (
    <div className="RSHD-menu-node">
      <h5 className="RSHD-menu-node-header">{header}</h5>

      <div className="grid xsmall-block-3 RSHD-menu-node-cols">
        {nodes.map((nodeItem, index) =>
          <ul
            key={index}
            className="cell RSHD-menu-node-col"
          >
              <li key={nodeItem.url}>
                <a
                  href={prependUrl + nodeItem.url}
                  className="RSHD-menu-node-item"
                >
                  {nodeItem.name}
                  {nodeItem.count && <span className="RSHD-menu-node-count"> ({nodeItem.count})</span>}
                </a>
              </li>
          </ul>
        )}
      </div>
    </div>
  )
}

export default Node