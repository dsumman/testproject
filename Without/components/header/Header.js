import React, { Component, Fragment } from "react";
import Head from "next/head";
import PrimaryNav from "./PrimaryNav";
import SecondaryNav from "./SecondaryNav";
import PredictiveSearch from './PredictiveSearch';
import Login from "./Login";
import Basket from "./Basket";

class Header extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.state = {
      active: null
    };
  }

  handleClick(event, position) {
    event.preventDefault();
    this.setState({
      ...this.state,
      active: this.state.active === position ? null : position
    });
  }

  render() {
    try {
      return (
        <Fragment>
          {this.state.active !== null && (
            <div
              className="RSHD-overlay"
              onClick={event => this.handleClick(event, null)}
            />
          )}

          <header className="RSHD">
            <div className="grid-container RSHD-content">
              <h1 className="RSHD-logo">
                <a href="/web/" className="RSHD-logo-link">
                  RS Components
                </a>
              </h1>
              <PrimaryNav
                menuItems={this.props.data.localeDataStore.menu}
                clickHandler={this.handleClick}
                active={this.state.active}
                rootUrl={"https://uk.rs-online.com/web/c/"}
              />
              <SecondaryNav
                menuItems={[
                  {
                    name: this.props.data.localeDataStore.header.headerHelp,
                    url: "/web/generalDisplay.html?id=support/support"
                  },
                  {
                    name: this.props.data.localeDataStore.header.solutions,
                    url: "/web/generalDisplay.html?id=solutions"
                  },
                  {
                    name: this.props.data.localeDataStore.header.headerInspiredTxt,
                    url:
                      "/web/generalDisplay.html?id=fortheinspired&amp;intcmp=UK-WEB-_-HP-HL-_-BRA_045_1117BR_FTIHub-_-fortheinspired"
                  },
                  {
                    name: this.props.data.localeDataStore.header.headerTradeCounters,
                    url:
                      "/web/generalDisplay.html?id=our-services/branch-network"
                  }
                ]}
              />
              <PredictiveSearch
                placeholder={
                  this.props.data.localeDataStore.header.Cat_SearchBoxDefaultText
                }
                locale={this.props.locale}
              />
              <Login
                labelData={[
                  {
                    name: this.props.data.localeDataStore.header.headerLogIn,
                    url: "/web/authHome.html"
                  },
                  {
                    name: this.props.data.localeDataStore.header.register,
                    url: "/web/register/registration"
                  }
                ]}
              />
              <Basket host={this._host} cookie={this._cookie} />
            </div>
            <Head>
              <title>Raspberry Pi | RS Components</title>
              {/*<style dangerouslySetInnerHTML={{ __html: stylesheet }} />*/}
              <link href="/static/css/sub.css" rel="stylesheet" />
              <link href="/static/css/custom.css" rel="stylesheet" />
            </Head>
          </header>
        </Fragment>
      );
    } catch (e) {
      return <div />;
    }
  }
}

export default Header;
