import React from "react";

import Node from "./Node";

const Menu = ({ menuItems, position, active, prependUrl }) => {
  const isProductMenu = position === 0;
  const isBrandMenu = position === 1;

  return (
    <ul
      className={`RSHD-menu-sub ${
        isProductMenu ? "RSHD-menu-product-sub" : ""
      } ${isBrandMenu ? "RSHD-menu-brand-sub" : ""}`}
      style={{ display: active ? "block" : "none" }}
    >
      {menuItems.map((sub, index) => (
        <li
          key={index}
          className={`RSHD-menu-sub-block ${
            isProductMenu
              ? `RSHD-menu-product-block RSHD-menu-product-block${index}`
              : ""
          }`}
        >
          {sub.name && (
            <h4 className="RSHD-menu-sub-header">
              {!sub.url && sub.name}
              {sub.url && <a href={prependUrl + sub.url}>{sub.name}</a>}
            </h4>
          )}
          {sub.menus &&
            !isBrandMenu && (
              <ul className="RSHD-menu-sub-list">
                {sub.menus.map(item => (
                  <li
                    key={item.url}
                    className={`RSHD-menu-sub-hover ${
                      isProductMenu ? "RSHD-menu-node-hover" : ""
                    } ${
                      isProductMenu && index > 1
                        ? "RSHD-menu-node-hover-up"
                        : ""
                    }`}
                  >
                    <a
                      href={prependUrl + item.url}
                      className={`RSHD-menu-sub-item ${
                        isProductMenu ? "RSHD-menu-product-hover" : ""
                      }`}
                    >
                      {item.name}
                    </a>
                    {isProductMenu && (
                      <Node
                        header={item.name}
                        nodes={item.menus}
                        prependUrl={prependUrl}
                      />
                    )}
                  </li>
                ))}
              </ul>
            )}
          {sub.menus &&
            isBrandMenu && (
              <ul className="grid xsmall-block-5 RSHD-menu-brands">
                {sub.menus.map(brand => (
                  <li key={brand.url} className="cell RSHD-menu-brand">
                    <a
                      href={brand.url}
                      className={`RSHD-menu-brand-item RSHD-sprite RSHD-sprite-brand${
                        brand.id
                      }`}
                      title={brand.name}
                    >
                      {brand.name}
                    </a>
                  </li>
                ))}
              </ul>
            )}
        </li>
      ))}
    </ul>
  );
};

export default Menu;
