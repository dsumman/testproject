import React from 'react'

const SecondaryNav = ({ menuItems }) => {
  return (
    <ul className="RSHD-secondary">
      { menuItems.map((menuItem, index) =>
        <li key={menuItem.url + index} className="RSHD-secondary-block">
          <a href={menuItem.url} className="RSHD-secondary-link">{menuItem.name}</a>
        </li>
      )}
    </ul>
  )
}

export default SecondaryNav