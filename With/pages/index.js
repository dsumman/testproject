import React from "react";
import App from "../app-shells/AppFullServerSideRendered";
import data from "../data/menu";

export default class extends React.Component {
  static async getInitialProps(ctx) {
    return { title: "Hello Worlds", data };
  }

  render() {
    return <App {...this.props} />;
  }
}
