import Header from '../components/header/Header';
import Head from "next/head";

export default ({ children, locale, data }) => (
  <div className="RSLC">
    <Head>
      {/*<link href="/static/css/sub.css" rel="stylesheet" />*/}
      {/*<link href="/static/css/custom.css" rel="stylesheet" />*/}
    </Head>
    <Header locale={'uk'} data={data}/>
    {children}
    <style jsx global>{``}</style>
  </div>
);
