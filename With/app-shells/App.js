import Head from "next/head";

export default class App extends React.Component {
  render() {
    return (
      <div className="RSLC">
        <Head>
          {/*<link href="/static/css/sub.css" rel="stylesheet" />*/}
          {/*<link href="/static/css/custom.css" rel="stylesheet" />*/}
        </Head>

        <main>{this.props.children}</main>
      </div>
    );
  }
}
