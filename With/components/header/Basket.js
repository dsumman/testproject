import React from 'react';

export default class Basket extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quantity: 0,
      price: '£0.00',
      stable: 'stable'
    };
  }

  async getData() {
    if (process.browser) {
      try {
        const response = await fetch('/web/miniBasketRunningTotal.html', {
          credentials: 'same-origin'
        });
        const data = await response.text();

        let parts;
        if (data) {
          parts = data.split('||');
        }

        if (parts && parts.length && parts.length > 2) {
          this.setState({ quantity: parts[1], price: parts[2], stable: parts[0] });
        }
      } catch (e) {
        console.log('Failed : Basket fetch', e);
      }
    }
  }

  async componentDidMount() {
    await this.getData();
  }

  render() {
    return (
      <a href="ca/basketsummary/" className="RSHD-basket">
        <span className="RSHD-basket-quantity">{this.state.quantity}</span>
        {this.state.price}
      </a>
    );
  }
}
