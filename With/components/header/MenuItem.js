import React, { Component } from 'react';

import IconChevron from '../Icons/IconChevron';
import Menu from './Menu';

class MenuItem extends Component {
  constructor(props) {
    super(props);
    this.state = { hover: false };

    this.mouseHandler = this.mouseHandler.bind(this);
  }

  mouseHandler() {
    this.setState({ hover: !this.state.hover });
  }

  render() {
    return (
      <li className="RSHD-menu-block">
        <a
          href={this.props.menuItems ? '' : this.props.url}
          onClick={(event) => this.props.menuItems && this.props.clickHandler(event, this.props.position)}
          onMouseEnter={this.mouseHandler}
          onMouseLeave={this.mouseHandler}
          className={`RSHD-menu-item ${this.props.active ? 'RSHD-menu-item-active' : ''}`}
        >
          {this.props.label}
          {this.props.menuItems && <IconChevron title={this.props.label} className="RSHD-menu-item-icon" direction="down" fill={this.state.hover ? '#ef0000' : '#fff'} />}
        </a>
        {this.props.menuItems && <Menu menuItems={this.props.menuItems} active={this.props.active} position={this.props.position} prependUrl={this.props.prependUrl} />}
      </li>
    );
  }
}

export default MenuItem;
