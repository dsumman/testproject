import React, { Fragment } from 'react';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: '',
      logoutLink: '',
      loggedIn: false,
      greeting: '',
      logoutLabel: ''
    };
  }

  async getData() {
    if (process.browser) {
      try {
        const response = await fetch('/web/services/header/getDynamicData', {
          credentials: 'same-origin'
        });
        const data = await response.json();
        this.setState({
          firstname: data.names.foreName,
          logoutLink: data.loggedInfo.logoutLink,
          loggedIn: data.loggedInfo.loggedIn,
          greeting: data.names.greeting,
          logoutLabel: data.loggedInfo.logoutLabel,
          rememberMe: data.rememberMe
        });
      } catch (e) {
        console.log('Failed : Logged in user details');
      }
    }
  }

  async componentWillMount() {
    await this.getData();
  }
  render() {
    if (this.state.rememberMe && this.state.rememberMe.selected) {
      return (
        <ul className="RSHD-login">
          <li className="RSHD-login-block">
            <a href={this.state.rememberMe.logInLink} className="RSHD-login-item">
              {this.state.rememberMe.logInLabel}
            </a>
          </li>
          <li className="RSHD-login-block" style={{ color: '#1e5787' }}>
            {this.state.greeting + ' ' + this.state.firstname}
          </li>
          <li className="RSHD-login-block">
            <a href={this.state.rememberMe.notMeLink} className="RSHD-login-item">
              {this.state.rememberMe.notMeLabel}
            </a>
          </li>
        </ul>
      );
    }

    if (this.state.loggedIn) {
      return (
        <ul className="RSHD-login">
          <li className="RSHD-login-block">
            <a href={this.state.logoutLink} className="RSHD-login-item">
              {this.state.logoutLabel}
            </a>
          </li>
          <li className="RSHD-login-block" style={{ color: '#1e5787' }}>
            {this.state.greeting + ' ' + this.state.firstname}
          </li>
        </ul>
      );
    }

    return (
      <ul className="RSHD-login">
        {this.props.labelData.map((item) => {
          return (
            <li key={item.url} className="RSHD-login-block">
              <a href={item.url} className="RSHD-login-item">
                {item.name}
              </a>
            </li>
          );
        })}
      </ul>
    );
  }
}
