import React from "react";
import styled from "styled-components";

import Node from "./Node";

const Ul = styled.ul`
  background: #fff;
  border-top: 4px solid #ef0000;
  font-size: 14px;
  left: 0;
  line-height: 20px;
  list-style: none;
  padding: 5px;
  position: absolute;
  top: 32px;
  width: 310px;
  z-index: 51;
  
  ${props => (props.isProductMenu ? "padding-left: 10px; width: 345px;" : "")};
  ${props => (props.isBrandMenu ? "width: 607px;" : "")};
  display: ${props => (props.active ? "block" : "none")};
} 
`;

const Menu = ({ menuItems, position, active, prependUrl }) => {
  const isProductMenu = position === 0;
  const isBrandMenu = position === 1;

  return (
    <Ul isProductMenu={isProductMenu} isBrandMenu={isBrandMenu} active={active}>
      {menuItems.map((sub, index) => (
        <li
          key={index}
          className={`RSHD-menu-sub-block ${
            isProductMenu
              ? `RSHD-menu-product-block RSHD-menu-product-block${index}`
              : ""
          }`}
        >
          {sub.name && (
            <h4 className="RSHD-menu-sub-header">
              {!sub.url && sub.name}
              {sub.url && <a href={prependUrl + sub.url}>{sub.name}</a>}
            </h4>
          )}
          {sub.menus &&
            !isBrandMenu && (
              <ul className="RSHD-menu-sub-list">
                {sub.menus.map(item => (
                  <li
                    key={item.url}
                    className={`RSHD-menu-sub-hover ${
                      isProductMenu ? "RSHD-menu-node-hover" : ""
                    } ${
                      isProductMenu && index > 1
                        ? "RSHD-menu-node-hover-up"
                        : ""
                    }`}
                  >
                    <a
                      href={prependUrl + item.url}
                      className={`RSHD-menu-sub-item ${
                        isProductMenu ? "RSHD-menu-product-hover" : ""
                      }`}
                    >
                      {item.name}
                    </a>
                    {isProductMenu && (
                      <Node
                        header={item.name}
                        nodes={item.menus}
                        prependUrl={prependUrl}
                      />
                    )}
                  </li>
                ))}
              </ul>
            )}
          {sub.menus &&
            isBrandMenu && (
              <ul className="grid xsmall-block-5 RSHD-menu-brands">
                {sub.menus.map(brand => (
                  <li key={brand.url} className="cell RSHD-menu-brand">
                    <a
                      href={brand.url}
                      className={`RSHD-menu-brand-item RSHD-sprite RSHD-sprite-brand${
                        brand.id
                      }`}
                      title={brand.name}
                    >
                      {brand.name}
                    </a>
                  </li>
                ))}
              </ul>
            )}
        </li>
      ))}
    </Ul>
  );
};

export default Menu;
