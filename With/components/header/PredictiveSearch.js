import React from 'react';

// export const GET_GENERAL_DISPLAY_PAGE = gql`
//   query predictiveSearch($searchTerm: String!, $locale: localeEnum!) {
//     predictiveSearch(searchTerm: $searchTerm, locale: $locale) {
//       resultsFound
//       categories {
//         label
//         results {
//           l1
//           l2
//           url
//           productResources {
//             productNumber
//             brandName
//             longDescription
//             imagePath
//           }
//         }
//       }
//       brands {
//         label
//         results {
//           name
//           url
//         }
//       }
//       partNumbers {
//         label
//         results {
//           name
//           url
//         }
//       }
//     }
//   }
// `;

class PredictiveSearch extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: '',
      showTopProducts: false,
      showPredicitveSearch: false,
      topProducts: []
    };

    this.categoriesHover = this.categoriesHover.bind(this);
    this.hideTopProducts = this.hideTopProducts.bind(this);
    this.showPredictiveSearch = this.showPredicitveSearch.bind(this);
    this.hidePredicitveSearch = this.hidePredicitveSearch.bind(this);
    this.formSubmit = this.formSubmit.bind(this);
  }

  formSubmit(e) {
    if (e && e.target && e.target.length === 4) {
      if (e.target[0].value.length === 1) {
        alert('Please enter a valid search term.');
        e.preventDefault();
      }
    }
  }

  categoriesHover(e, item) {
    this.setState({ showTopProducts: true, topProducts: item.productResources, topProduct: item.l2 });
  }

  hideTopProducts(e) {
    this.setState({ showTopProducts: false });
  }

  hidePredicitveSearch(event) {
    this.setState({ value: event.target.value, showPredicitveSearch: false, showTopProducts: false });
  }

  showPredicitveSearch(event) {
    let searchTerm = '';
    if (event.target.value) searchTerm = event.target.value.trim();

    this.setState({ value: searchTerm, showPredicitveSearch: event.target.value.length >= 3, showTopProducts: false });
  }

  renderSingleLink(text, link) {
    let escapedTextboxValue = escapeRegexString(this.state.value);
    const rx = new RegExp('(.*)(' + escapedTextboxValue + ')(.*)', 'i');
    const match = rx.exec(text);

    if (!match) return text;

    return (
      <a href={link}>
        {match[1]}
        <strong>{match[2]}</strong>
        {match[3]}
      </a>
    );
  }

  render() {
    return (
      <div onMouseLeave={(event) => this.hidePredicitveSearch(event)}>
        <form className="RSHD-search" name="searchForm" method="get" action="/web/c">

          <input type="text" name="searchTerm" value="" className={"RSHD-search-input"}/>

          {/*<DebounceInput*/}
          {/*  type="text"*/}
          {/*  name="searchTerm"*/}
          {/*  autoComplete="off"*/}
          {/*  placeholder={this.props.placeholder}*/}
          {/*  className="RSHD-search-input"*/}
          {/*  minLength={3}*/}
          {/*  debounceTimeout={300}*/}
          {/*  onChange={(event) => this.showPredictiveSearch(event)}*/}
          {/*/>*/}
          <input type="hidden" name="sra" value="oss" />
          <input type="hidden" name="r" value="t" />
          <input type="submit" alt="Find" value="" className="RSHD-search-button" />

        </form>
      </div>
    );
  }
}

export default PredictiveSearch;
