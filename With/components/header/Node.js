import React from "react";
import styled from "styled-components";

const RshdMenuNode = styled.div`
  background: #fff;
  border: 1px solid #ed1d24;
  box-shadow: 0 3px 5px #ccc;
  display: none;
  line-height: 20px;
  margin: 0;
  padding: 15px 20px;
  position: absolute;
  right: -662px;
  //top: -10px;
  width: 662px;
  z-index: 53;
  bottom: -10px;
  top: auto;

  ::before {
    content: "";
    border-style: solid;
    border-width: 9px 16px 9px 0;
    border-color: transparent #ed1d24 transparent transparent;
    position: absolute;
    left: -16px;
    top: 14px;
    z-index: 52;
  }

  ::after {
    content: "";
    border-style: solid;
    border-width: 8px 15px 8px 0;
    border-color: transparent #fff transparent transparent;
    position: absolute;
    left: -14px;
    top: 15px;
    z-index: 52;
  }
`;

const RSHDMenuNodeHeader = styled.h5`
  color: #1e5787;
  font-size: 14px;
  font-weight: normal;
  margin: 0;
`;

const Div = styled.div`
  -ms-flex-flow: row wrap;
  -webkit-box-direction: normal;
  -webkit-box-orient: horizontal;
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  flex-flow: row wrap;
  width: 33.33333%;
  margin: 0;
`;

const Node = ({ header, nodes, prependUrl }) => {
  return (
    <RshdMenuNode>
      <RSHDMenuNodeHeader>{header}</RSHDMenuNodeHeader>

      <Div>
        {nodes.map((nodeItem, index) => (
          <ul key={index} className="cell RSHD-menu-node-col">
            <li key={nodeItem.url}>
              <a
                href={prependUrl + nodeItem.url}
                className="RSHD-menu-node-item"
              >
                {nodeItem.name}
                {nodeItem.count && (
                  <span className="RSHD-menu-node-count">
                    {" "}
                    ({nodeItem.count})
                  </span>
                )}
              </a>
            </li>
          </ul>
        ))}
      </Div>
    </RshdMenuNode>
  );
};

export default Node;
