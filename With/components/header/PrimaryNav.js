import React from 'react'

import MenuItem from './MenuItem'

const PrimaryNav = ({ menuItems, active, clickHandler, content }) => {

  return (
    <nav>
      <ul className={`RSHD-menu ${active !== null ? 'RSHD-menu-active' : ''}`}>
        <MenuItem
          label={menuItems.navMenu1.name}
          url={""}
          prependUrl={"c/"}
          menuItems={menuItems.navMenu1.menus}
          position={0}
          clickHandler={clickHandler}
          active={active === 0}
        />
        <MenuItem
          label={menuItems.navMenu2.name}
          url={""}
          prependUrl={""}
          menuItems={menuItems.navMenu2.menus}
          position={1}
          clickHandler={clickHandler}
          active={active === 1}
        />
        <MenuItem
          label={menuItems.navMenu3.name}
          url={"https://uk.rs-online.com/web/generalDisplay.html?id=new"}
          prependUrl={""}
          menuItems={null}
          position={2}
          clickHandler={clickHandler}
          active={active === 2}
        />
        <MenuItem
          label={menuItems.navMenu4.name}
          url={""}
          prependUrl={""}
          menuItems={menuItems.navMenu4.menus}
          position={3}
          clickHandler={clickHandler}
          active={active === 3}
        />
        <MenuItem
          label={menuItems.navMenu5.name}
          url={"https://uk.rs-online.com/web/generalDisplay.html?id=our-services/our-services"}
          prependUrl={""}
          menuItems={null}
          position={4}
          clickHandler={clickHandler}
          active={active === 4}
        />


        {/*})}*/}
      </ul>
    </nav>
  )
}
export default PrimaryNav
